#![feature(proc_macro_hygiene, decl_macro)]

use frank_jwt::{decode, encode, validate_signature, Algorithm, ValidationOptions};
use health_monitor::start_server;
use quenbyaakot_core::types::{self, ChallengeConfig, Role};
use rocket::{
    get,
    http::{Cookie, Cookies, Status},
    request::{self, FromRequest, Request},
    routes, Outcome, State,
};
use rocket_contrib::{json::Json, serve::StaticFiles};
use serde::{Deserialize, Serialize};
use serde_json::json;
use std::{env, fs::read_to_string, net::IpAddr};
use rand::seq::SliceRandom;
use rocket_cors::{CorsOptions, AllowedOrigins};

const GUEST_NAMES: [&str; 10] = [
    "The Clever Pigs",
    "The Golden Komodos",
    "The Steel Yetis",
    "The Monstrous Striders",
    "The Bold Soldiers",
    "The White Martians",
    "The Courageous Crabs",
    "The Infamous Dogs",
    "The Colossal Swans",
    "The Enchanted Yetis",
];

#[derive(Serialize)]
enum AuthResult {
    NotFound,
    Ok,
    Error(String),
}

#[derive(Serialize)]
struct AuthResponse {
    result: AuthResult,
}

#[derive(Serialize, Deserialize)]
enum UserRole {
    Guest,
    Red,
    Blue,
}

#[derive(Debug)]
enum RoleError {
    JWTInvalid,
}

impl<'a, 'r> FromRequest<'a, 'r> for UserRole {
    type Error = RoleError;

    fn from_request(request: &'a Request<'r>) -> request::Outcome<Self, Self::Error> {
        let cookies = request.cookies();
        let jwt = match cookies.get("jwt") {
            Some(cookie) => cookie.value(),
            None => return Outcome::Success(UserRole::Guest),
        };

        let secret = env!("SECRET");

        if Ok(true) != validate_signature(jwt, &secret.to_string(), Algorithm::HS256) {
            return Outcome::Failure((Status::BadRequest, RoleError::JWTInvalid));
        }

        let body = match decode(
            jwt,
            &secret.to_string(),
            Algorithm::HS256,
            &ValidationOptions::dangerous(),
        ) {
            Ok((_, body)) => body,
            Err(_) => return Outcome::Failure((Status::BadRequest, RoleError::JWTInvalid)),
        };

        match serde_json::from_value::<UserRole>(body) {
            Ok(role) => Outcome::Success(role),
            _ => Outcome::Success(UserRole::Guest),
        }
    }
}

#[derive(Serialize)]
struct TaskResponse {
    result: Result<TaskInfo, String>,
}

#[derive(Serialize)]
struct TaskInfo {
    addr: IpAddr,
    name: String,
    points: i16,
    icon: String,
    protected: ProtectedInfo,
}

#[derive(Serialize)]
#[serde(untagged)]
enum ProtectedInfo {
    Red {
        description: String,
    },
    Blue {
        description: String,
        password: String,
    },
    Guest,
}

#[derive(Serialize)]
struct InfoResponse {
    name: String,
    role: UserRole,
}

#[get("/")]
fn index() -> &'static str {
    "Hello, world!"
}

#[get("/api/auth?<username>&<password>")]
fn auth(
    config: State<ChallengeConfig>,
    mut cookies: Cookies,
    username: Option<String>,
    password: Option<String>,
) -> Json<AuthResponse> {
    if username.is_none() {
        return Json(AuthResponse {
            result: AuthResult::Error("error: username required".to_string()),
        });
    }

    if password.is_none() {
        return Json(AuthResponse {
            result: AuthResult::Error("error: password required".to_string()),
        });
    }

    let username = username.unwrap();
    let password = password.unwrap();

    for team in config.teams.iter() {
        if team.name == username && team.password == password {
            let role = match team.role {
                types::Role::Attacker => UserRole::Red,
                types::Role::Defender => UserRole::Blue,
            };

            let header = json!({});
            let secret = env!("SECRET");
            let jwt = encode(
                header,
                &secret.to_string(),
                &serde_json::to_value(role).unwrap(),
                Algorithm::HS256,
            )
            .unwrap();

            cookies.add(Cookie::new("jwt", jwt));
            return Json(AuthResponse {
                result: AuthResult::Ok,
            });
        }
    }

    Json(AuthResponse {
        result: AuthResult::NotFound,
    })
}

#[get("/api/task/<machine_ip>")]
fn task(config: State<ChallengeConfig>, role: UserRole, machine_ip: IpAddr) -> Json<TaskResponse> {
    let machine = match config
        .machines
        .iter()
        .find(|machine| machine.address == machine_ip)
    {
        Some(machine) => machine,
        None => {
            return Json(TaskResponse {
                result: Err("error: machine with given ip not found".to_string()),
            })
        }
    };

    let protected = match role {
        UserRole::Blue => ProtectedInfo::Blue {
            description: machine.description.clone(),
            password: machine.password.clone(),
        },
        UserRole::Red => ProtectedInfo::Red {
            description: machine.description.clone(),
        },
        UserRole::Guest => ProtectedInfo::Guest,
    };

    Json(TaskResponse {
        result: Ok(TaskInfo {
            addr: machine.address,
            name: machine.name.clone(),
            points: machine.points,
            icon: machine.icon.clone(),
            protected,
        }),
    })
}

#[get("/api/info")]
fn info(config: State<ChallengeConfig>, role: UserRole) -> Json<InfoResponse> {
    Json(match role {
        UserRole::Blue => InfoResponse {
            name: config
                .teams
                .iter()
                .find(|team| team.role == Role::Defender)
                .unwrap()
                .name.clone(),
            role: UserRole::Blue,
        },
        UserRole::Red => InfoResponse {
            name: config
                .teams
                .iter()
                .find(|team| team.role == Role::Attacker)
                .unwrap()
                .name.clone(),
            role: UserRole::Red,
        },
        UserRole::Guest => InfoResponse {
            name: (*GUEST_NAMES.choose(&mut rand::thread_rng()).unwrap()).to_string(),
            role: UserRole::Guest,
        },
    })
}

#[tokio::main]
async fn main() {
    let args: Vec<String> = env::args().collect();

    if args.len() != 2 {
        eprintln!("error: missing config path argument");
        return;
    }

    let config = read_to_string(&args[1]).expect("error: cannot read config");
    let config: ChallengeConfig =
        serde_yaml::from_str(&config).expect("error: invalid config file");

    tokio::spawn(start_server(config.clone()));

    rocket::ignite()
        .manage(config)
        .mount("/", routes![index, auth, task, info])
        .mount("/imgs/", StaticFiles::from("../web-interface/build/images/"))
        .attach(CorsOptions {
            allowed_origins: AllowedOrigins::All,
            ..Default::default()
        }.to_cors().unwrap())
        .launch();
}
