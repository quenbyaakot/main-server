# CastleWars
Red-Blue battle infrastrusture
## About
![screenshot](https://i.ibb.co/bBBG67K/localhost-3000.png")
[Red-Blue](https://securitytrails.com/blog/cybersecurity-red-blue-team) battle cybersecurity competition infrastrusture.

Access to a target server with some exploits is given to the blue team, which need to protect it from red team's attacks by patching the vulns.

The red team's task is to gain root access and shut the machine down.
If they succeed in bringing it down in the given time and keeping it down, they have won.

Every target machine is assigned a certain amount of coins, and at the end they're all added up to determine the winning team.

The main server provides a dashboard displaying all host's current status and a history of all events, as well as custom information for each team, such as ssh passwords for blue and hints for red.

### Built With
* [Rust](https://www.rust-lang.org/)
* [rocket.rs](https://rocket.rs/)
* [React](https://reactjs.org/)
## Getting Started
### Prerequisites
* npm
* rustup
### Installation
#### Main server
1. `git clone https://gitlab.com/quenbyaakot/main-server`
2. `git clone https://gitlab.com/quenbyaakot/web-interface`
3. `cd ./web-interface`
4. `npm run build`
5. `cd ../main-server`
6. `rustup override set nightly` (required by rocket.rs)
#### Target machines
Please refer to [reporter](https://gitlab.com/quenbyaakot/health-reporter).
## Usage
1. `export SECRET=<jwt_secret>`
2. `cargo r </path/to/the/config>`
